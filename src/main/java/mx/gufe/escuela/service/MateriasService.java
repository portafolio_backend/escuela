package mx.gufe.escuela.service;

import java.util.List;

import mx.gufe.escuela.model.Alumno;
import mx.gufe.escuela.model.Materia;
import mx.gufe.escuela.utils.EscuelaException;

public interface MateriasService {
	List<Materia> allMaterias() throws EscuelaException;
	Materia materia(Integer idMateria) throws EscuelaException;
	
	public Materia crearMateria(Materia crearMateria);
	public Materia buscarMateriaPorId(Integer id);
	public Materia buscarMateriaPorNombre(String materia);
	public Materia actualizarMateriaPorId(Integer id,Materia actualizarMateria);
	public Boolean eliminarMateriaPorId(Integer id);
	
	
	
	public Materia eliminadoLogico(Integer id);
	public List<Materia> listarMateriaporEstado(Long estado);
	public List<Materia> listarMateriaEliminado();
	 
	
}
