package mx.gufe.escuela.service;

import java.util.List;

import mx.gufe.escuela.dto.BoletaDTO;
import mx.gufe.escuela.dto.CalificacionDTO;
import mx.gufe.escuela.dto.HistorialDto;
import mx.gufe.escuela.model.Calificacion;
import mx.gufe.escuela.utils.EscuelaException;
import mx.gufe.escuela.utils.OveralResponse;

public interface CalificacionesService {
	BoletaDTO obtenerBoleta(Integer idAlumno) throws EscuelaException;
	OveralResponse addCalificacion(CalificacionDTO calificacionDto) throws EscuelaException;
	OveralResponse updCalificacion(CalificacionDTO calificacionDto) throws EscuelaException;
	OveralResponse delCalificacion(Integer idCalificacion) throws EscuelaException;
	
	Calificacion buscarCalificacionPorId(Integer id);
	public HistorialDto obtenerHistorial(Integer idAlumno);
}
