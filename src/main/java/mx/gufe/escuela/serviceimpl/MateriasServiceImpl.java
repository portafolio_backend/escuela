package mx.gufe.escuela.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.gufe.escuela.model.Materia;
import mx.gufe.escuela.repositories.MateriaRepository;
import mx.gufe.escuela.service.MateriasService;
import mx.gufe.escuela.utils.EscuelaException;

@Service
public class MateriasServiceImpl implements MateriasService {

	final static Logger logger = Logger.getLogger(MateriasServiceImpl.class.getName());
	
	@Autowired
	private MateriaRepository materiaRepository;
	
	@Override
	public List<Materia> allMaterias() throws EscuelaException{
		try {
			List<Materia> lstResp = materiaRepository.findAll();
			return lstResp;
		}catch(Exception e) {
			logger.error("Error al consultar materias: " + e.getMessage());
			throw new EscuelaException("Error al consultar materias.", e.getMessage());
		}
	}

	@Override
	public Materia materia(Integer idMateria) throws EscuelaException {
		try {
			Materia resp = materiaRepository.getReferenceById(idMateria);
			return resp;
		}catch(Exception e) {
			logger.error("Error al consultar materia: " + e.getMessage());
			throw new EscuelaException("Error al consultar materia.", e.getMessage());
		}
	}

	@Override
	public Materia crearMateria(Materia crearMateria) {
		Materia materia = new Materia();
		
		materia.getId();
		materia.setName(crearMateria.getName());
		materia.setActivo(crearMateria.getActivo());
		materia.setEstado(crearMateria.getEstado());

		materiaRepository.save(materia);
		
		return crearMateria;
	}

	@Override
	public Materia buscarMateriaPorId(Integer id) {
		return materiaRepository.findById(id).orElse(null);
	}
	
	@Override
	public Materia buscarMateriaPorNombre(String materia) {
		Optional<Materia> materiaOptional = materiaRepository.buscarPorNombre(materia);
		if (materiaOptional.isPresent()) {
			return materiaOptional.get();
		}else {
		return null;
		}
	}
	@Override
	public Materia actualizarMateriaPorId(Integer id, Materia actualizarMateria) {
		Materia materia = buscarMateriaPorId(id);
		if(materia != null) {
			materia.setName(actualizarMateria.getName());
			materiaRepository.save(materia);
			return actualizarMateria;
		}else {
		return null;
		}
	}

	@Override
	public Boolean eliminarMateriaPorId(Integer id) {
		if(buscarMateriaPorId(id) != null) {
			materiaRepository.deleteById(id);
			return true;
		}else {
		return false;
		}
	}

	@Override
	public Materia eliminadoLogico(Integer id) {
		Materia materia = buscarMateriaPorId(id);
		if(materia != null) {
			materia.setEstado(2L);
			return materiaRepository.save(materia);
			 
		}else {
		return null;
		}
	}

	@Override
	public List<Materia> listarMateriaporEstado(Long estado) {
		return materiaRepository.listarPorEstado(estado);
	}

	@Override
	public List<Materia> listarMateriaEliminado() {
		return materiaRepository.listarEliminados();
	}


}
