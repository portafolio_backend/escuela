package mx.gufe.escuela.repositories;

import java.util.List;
import java.util.Optional;

import javax.websocket.server.PathParam;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import mx.gufe.escuela.model.Materia;

public interface MateriaRepository extends JpaRepository<Materia, Integer> {

	@Query ( value = "SELECT * FROM t_materias tm WHERE tm.nombre = ?1",nativeQuery = true)
	Optional<Materia> buscarPorNombre(@PathParam("name") String name);

	@Query ( value = "SELECT * FROM t_materias tm WHERE tm.estado = :estado",nativeQuery = true)
	List<Materia> listarPorEstado(@Param("estado") Long estado);
	
	@Query ( value = "SELECT * FROM t_materias tm WHERE tm.estado = 1",nativeQuery = true)
	List<Materia> listarEliminados();

}
