package mx.gufe.escuela.dto;

import java.io.Serializable;

import lombok.Data;
import mx.gufe.escuela.model.Alumno;

@Data
public class DatosDTO implements Serializable{

	private Integer idAlumno;
	private String nombre;
	private String apellido;
	private String fecha_registro;
	
	private static final long serialVersionUID = -5590763124639412648L;

}
