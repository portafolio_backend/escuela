package mx.gufe.escuela.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class MateriasDto implements Serializable{
	
	private Integer idCalificacion;
	private Integer idMateria;
	private String materia;
	private Double calificacion;
	
	private static final long serialVersionUID = 5798506775698194638L;

}
