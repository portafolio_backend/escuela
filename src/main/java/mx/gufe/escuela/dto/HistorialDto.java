package mx.gufe.escuela.dto;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import mx.gufe.escuela.model.Alumno;

@Data
public class HistorialDto implements Serializable{
	

	private DatosDTO datos;
	private List<MateriasDto> materias;
	private PromedioDTO promedio;
	
	private static final long serialVersionUID = 3289344999519220974L;



}
