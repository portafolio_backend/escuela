package mx.gufe.escuela.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import mx.gufe.escuela.model.Alumno;
import mx.gufe.escuela.service.AlumnosService;
import mx.gufe.escuela.utils.EscuelaException;

//@Controller
@RestController
@RequestMapping("/alumnos")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET})
public class AlumnosController {
	@Autowired
	private AlumnosService alumnosService;
	
	@GetMapping("/findAll")
	public ResponseEntity<List<Alumno>> alumnos () {
		List<Alumno> resp = new ArrayList<>(); 
		try {
			resp = alumnosService.allAlumnos();
			return new ResponseEntity<>(resp, HttpStatus.OK);
		}catch( EscuelaException ee ) {
			return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
		}
	}
	
	@PostMapping("/create")
	public ResponseEntity<?> crearAlumno(@RequestBody Alumno crearAlumno){
		Map<String, Object> response = new HashMap<>();
		try {
			alumnosService.crearAlumno(crearAlumno);
		}catch (DataAccessException e) {
			response.put("Mensaje : ", "no se pudo crear el Alumno");
			response.put("[ERR] ", e.getMostSpecificCause().getMessage());

			return new ResponseEntity<Map<String, Object>>(response , HttpStatus.BAD_GATEWAY);
		}
		response.put("Mensaje  : ", "alumnoCreado Con Exito");
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@GetMapping("/findById/{id}")
	public ResponseEntity<?> buscarPorId(@PathVariable("id")Integer id){
		Alumno alumno = null;
		Map<String, Object> response = new HashMap<>();
		try {
			alumno = alumnosService.buscaPorId(id);
		}catch (DataAccessException e) {
			response.put("Mensaje ", "error al acceder a la base de datos");
			response.put("[ERR ] ", e.getMostSpecificCause().getCause());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}if ( alumno == null ) {
			response.put("mensaje ", "el Alumno con el id ".concat(id.toString().concat(" no existe en la base de datos ")));
			return new ResponseEntity<Map<String, Object>> (response, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Alumno>(alumno,HttpStatus.OK);
	}
	
	@DeleteMapping("/deleteById/{id}")
	public ResponseEntity<?> eliminarAlumno(@PathVariable("id")Integer id){
		Boolean alumno;
		Map<String, Object> response = new HashMap<>();
		try {
			alumno = alumnosService.eliminarAlumnoPorId(id);
		}catch (DataAccessException e) {
			response.put("Mensaje ", "error al acceder a la base de datos");
			response.put("[ERR ] ", e.getMostSpecificCause().getCause());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}if ( alumno == true ) {
			response.put("mensaje ", "el Alumno con el id ".concat(id.toString().concat(" ha sido eliminado de la  base de datos ")));
			return new ResponseEntity<Map<String, Object>> (response, HttpStatus.BAD_REQUEST);
		}else {
			response.put("mensaje ", "el Alumno con el id ".concat(id.toString().concat(" no existe en  la  base de datos ")));
			return new ResponseEntity<Map<String, Object>> (response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("/delelLogico/{id}")
	public ResponseEntity<?> borradoLogico(@PathVariable("id")Integer id){
		Alumno alumno = null;
		Map<String, Object> response = new HashMap<>();
		try {
			alumno = alumnosService.eliminadoLogico(id);
		}catch (DataAccessException e) {
			response.put("Mensaje ", "error al acceder a la base de datos");
			response.put("[ERR ] ", e.getMostSpecificCause().getCause());
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}if ( alumno == null ) {
			response.put("mensaje ", "el Alumno con el id ".concat(id.toString().concat(" no existe en la base de datos ")));
			return new ResponseEntity<Map<String, Object>> (response, HttpStatus.BAD_REQUEST);
		}
		response.put("mensaje ", "el Alumno con el id ".concat(id.toString().concat(" ha sido eliminado Logicamente de la  base de datos ")));
		return new ResponseEntity<Map<String, Object>> (response, HttpStatus.BAD_REQUEST);
	}
	
	@GetMapping("/findAllEstado/{estado}")
	public List<Alumno> listarporEstado(@PathVariable("estado") Integer estado) {
		return alumnosService.listarAlumnoporEstado(estado);
	}
	

}
