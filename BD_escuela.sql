create table t_alumnos (
	id_t_usuarios SERIAL not null,
	nombre varchar (80)not null,
	ap_paterno varchar(80)not null,
	ap_materno varchar(80)not null,
	activo int not null,
	estado int not null,
	unique (nombre,ap_paterno,ap_materno),
	primary key (id_t_usuarios)
);
insert into t_alumnos  (nombre,ap_paterno,ap_materno,activo,estado) values ('John','Dow','Down',1,1);

create table t_materias (
    id_t_materias SERIAL not null,
    nombre varchar(80) not null unique,
    activo int not null unique,
    estado int not null,
    primary key (id_t_materias)
);

insert into t_materias (nombre,activo,estado)  values ('programacion I',1,1),
										('ingenieria de sofware',2,1),
										('matematicas',3,1); 

create table t_calificaciones (
    id_t_calificaciones SERIAL not null,
    id_t_materias int not null,
    id_t_usuarios int not null,
    calificacion decimal (10,2),
    fecha_registro date not null,
    primary key (id_t_calificaciones),
    foreign key (id_t_materias) references t_materias (id_t_materias),
foreign key (id_t_materias) references t_materias (id_t_materias) );


select * from t_calificaciones tc ;
select * from t_alumnos ta  ;
select * from t_materias tm  ;


SELECT * FROM t_alumnos ta WHERE ta.estado = 2;
